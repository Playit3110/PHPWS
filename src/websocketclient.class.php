<?php
#[AllowDynamicProperties]
class WebSocketClient {
	private $GUID = "258EAFA5-E914-47DA-95CA-C5AB0DC85B11";

	public $socket;
	public $host = "0.0.0.0";
	public $port = 0;
	public $path = "/";

	public $headers = [];
	public $handshake = false;
	public $frames = [];
	public $messages = [];

	public function __construct(&$socket, $accept = true) {
		$this->socket = $socket;
		if($accept) {
			stream_set_blocking($this->socket, true);
			$this->socket = stream_socket_accept($this->socket);
			@stream_socket_enable_crypto($this->socket, true, STREAM_CRYPTO_METHOD_TLS_SERVER);
			stream_set_blocking($this->socket, false);

			//display information about the client who is connected
			if(($address = stream_socket_get_name($this->socket, true)) !== false) {
				[$this->host, $this->port] = explode(":", $address);
			}
		}
		stream_set_timeout($this->socket, 1);
	}

	public function parsePacket($packet, &$ws) {
		$lastFrame = array_pop($this->frames);
		if($lastFrame && !$lastFrame->frameEnd)
			$packet = $lastFrame->raw.$packet;

		$psize = strlen($packet);
		for($frameptr = 0; $frameptr < $psize;) {
			$frame = new Frame(substr($packet, $frameptr));
			switch($frame->headers["opcode"]) {
				case 0:
				case 1:
				case 2:
					break;
				case 8:
					$this->disconnect($ws);
					return;
				case 9:
					$this->send(null, "pong");
					break;
				case 10:
					break;
				default:
					$this->abort($ws);
					return;
			}

			$frameptr += $frame->getSize();

			$this->frames[] = $frame;
			if($frame->frameEnd && $frame->messageEnd) {
				$message = array_reduce($this->frames, function($pmsg, $frame) {
					return $pmsg.$frame->payload;
				});
				$this->frames = [];
				$ws->onmessage($this, $message);
			}
		}
	}

	public function handshake($buffer, &$ws) {
		if(strpos(
			$buffer,
			"\r\n\r\n"
		) === false) return;

		$headers = [];
		$lines = explode("\n", $buffer);
		$path = false;
		foreach($lines as $line) {
			if(strpos($line, ":") !== false) {
				$header = explode(":", $line, 2);
				$headers[strtolower(trim($header[0]))] = trim($header[1]);
			} elseif(stripos($line, "GET ") !== false) {
				preg_match("/GET (.*) HTTP/i", $buffer, $reqResource);
				$path = trim($reqResource[1]);
			}
		}
		if($path) {
			$this->path = $path;
		} else {
			if($ws->sslOnly)
				$error = $this->HTTPStatus(405);
		}

		if(
			(!isset($headers["host"]) || !$this->checkHost($headers["host"])) ||
			(!isset($headers["upgrade"]) || strtolower($headers["upgrade"]) != "websocket") ||
			(!isset($headers["connection"]) || strpos(strtolower($headers["connection"]), "upgrade") === false) ||
			(!isset($headers["sec-websocket-key"]))
		) {
			$error = $this->HTTPStatus(400);
		}

		if(!isset($headers["sec-websocket-version"]) || strtolower($headers["sec-websocket-version"]) != 13) {
			$error = implode("\r\n", [
				$this->HTTPStatus(426),
				"Sec-WebSocket-Version: 13"
			]);
		}

		if(isset($error)) {
			@fwrite($this->socket, $error);
			$this->abort($ws);
			return;
		}

		$this->handshake = true;

		$hash = sha1($headers["sec-websocket-key"].$this->GUID);
		$hash = base64_encode(
			array_reduce(
				str_split($hash, 2),
				function($lasthex, $hex) {
					return $lasthex.chr(hexdec($hex));
				}
			)
		);

		$prot = "";
		// if(isset($headers["sec-websocket-protocol"])) 
		// 	$prot = $this->processProtocol($headers["sec-websocket-protocol"]);
		$ext = "";
		// if(isset($headers["sec-websocket-extensions"]))
		// 	$ext = $this->processExtensions($headers["sec-websocket-extensions"]);

		$response = implode("\r\n", [
			$this->HTTPStatus(101),
			"Upgrade: websocket",
			"Connection: Upgrade",
			"Sec-WebSocket-Location: //".$ws->host.":".$ws->port.$this->path,
			"Sec-WebSocket-Accept: $hash",
			"$prot$ext",
			""
		]);
		@fwrite($this->socket, $response);
		$ws->onopen($this);
	}

	protected function checkHost($host) {
		return true;
	}

	private function HTTPStatus(int $status) {
		return implode(" ", [
			"HTTP/1.1",
			$status,
			[
				101 => "Switching Protocols",
				400 => "Bad Request",
				403 => "Forbidden",
				405 => "Method Not Allowed",
				426 => "Upgrade Required"
			][$status]
		]);
	}


	public function send($message, $type = "text") {
		if($this->handshake) {
			$message = Frame::encode($message, $this, $type);
			$result = @fwrite($this->socket, $message);
			return;
		}

		// User has not yet performed their handshake.
		// Store for sending later.
		$this->messages[] = $message;
	}


	public function disconnect(&$ws) {
		$this->send("", "close");

		$ws->onclose($this);
		$this->removeClient($ws);
		fclose($this->socket);
	}

	public function abort(&$ws, $error = null) {
		if($error)
			$ws->onerror($this, $error);

		$this->removeClient($ws);
	}

	public function removeClient(&$ws) {
		if(($i = array_search($this, $ws->clients)) !== false)
			unset($ws->clients[$i]);

		if(($i = array_search($this->socket, $ws->sockets)) !== false)
			unset($ws->sockets[$i]);
	}
}
?>