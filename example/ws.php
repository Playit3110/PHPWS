<pre>
<?php
set_time_limit(0);

require_once(dirname(__DIR__)."/src/websocket.class.php");
final class WS extends WebSocketServer {
	public function onstart() {
		return true;
	}

	public function onopen($client) {
		$client->send("Current Time: ".gmdate("Y-m-d H:i:s"));
		return true;
	}

	public function tick() {
		return true;
	}

	public function onmessage($own, $message) {
		foreach($this->clients as $client) {
			if($client !== $own)
				$client->send($message);
		}
		return true;
	}

	public function onclose($own) {
		foreach($this->clients as $client) {
			if($client !== $own)
				$client->send("User disconnected: ".$own->host);
		}
		return true;
	}

	public function onerror($client, $error) {
		return true;
	}
}

new WS();
?>
</pre>