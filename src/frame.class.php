<?php
class Frame {
	public $headers = [];
	public $frameEnd = true;
	public $messageEnd = false;
	public $raw;
	public $payload;

	public function __construct($message) {
		$this->parseHeaders($message);
		$this->raw = substr($message, 0, $this->getSize());
		
		// if($this->checkRSVBits()) {
		// 	return false;
		// }

		$this->payload = $this->unMask(
			substr($message, $this->headers["hlength"])
		);

		if($this->headers["plength"] > strlen($this->payload))
			$this->frameEnd = false;
	}

	protected function parseHeaders($message) {
		$this->headers = [
			"hlength"	=> 2,
			"fin"		=> ord($message[0]) >> 7 & 1,
			"rsv"		=> [
				ord($message[0]) >> 6 & 1,
				ord($message[0]) >> 5 & 1,
				ord($message[0]) >> 4 & 1
			],
			"opcode"	=> ord($message[0]) & (2 ** 4 - 1),
			"masked"	=> ord($message[1]) >> 7 & 1,
		];

		$this->messageEnd = $this->headers["fin"] == true;

		$this->headers["plength"] = ord($message[1]) & (2 ** 7 - 1);

		if($this->headers["plength"] == 126) {
			$this->headers["plength"] = ord($message[2]) * 2 ** 8 + ord($message[3]);
			$this->headers["hlength"] += 2;
		} elseif($this->headers["plength"] == 127) {
			$len = substr($message, 2, 8);
			$len = str_split($len);
			$len = array_reduce($len, function($lb, $b) {
				return $lb * 2 ** 8 + ord($b);
			});
			$this->headers["plength"] = $len;
			$this->headers["hlength"] += 8;
		}

		if($this->headers["masked"]) {
			$this->headers["mask"] = substr($message, $this->headers["hlength"], 4);
			$this->headers["hlength"] += 4;
		}
	}

	protected function unMask($payload) {
		if(!$this->headers["masked"])
			return $payload;

		$mask = str_pad("", strlen($payload), $this->headers["mask"]);
		return $payload ^ $mask;
	}

	public function getSize() {
		return $this->headers["hlength"] + $this->headers["plength"];
	}

	public function appendPayload($payload) {
		$this->payload .= $payload;
	}


	// override this method if you are using an extension where the RSV bits are used.
	protected function checkRSVBits() {
		if(
			ord($this->headers["rsv"][0]) +
			ord($this->headers["rsv"][1]) +
			ord($this->headers["rsv"][2]) > 0
		) {
			return true;
		}
		return false;
	}



	public static function encode($message, $client, $type = "text", $messageContinues = false) {
		$headers = [
			"fin"		=> 1,
			"rsv"		=> [0, 0, 0],
			"opcode"	=> [
				"continuous"=> 0,
				"text"		=> 1,
				"binary"	=> 2,
				"close"		=> 8,
				"ping"		=> 9,
				"pong"		=> 10
			][$type],
			"masked"	=> 0,
			"plength"	=> strlen($message),
			"payload"	=> $message
		];

		$message = [];
		$message[0] = $headers["fin"] << 7;
		$message[0] |= $headers["rsv"][0] << 6;
		$message[0] |= $headers["rsv"][1] << 5;
		$message[0] |= $headers["rsv"][2] << 4;
		$message[0] |= $headers["opcode"];

		if($headers["plength"] > 65536) {
			$headers["plength"] = [
				127,
				$headers["plength"] >> 24,
				$headers["plength"] >> 16 & (2 ** 8 - 1),
				$headers["plength"] >> 8 & (2 ** 8 - 1),
				$headers["plength"] & (2 ** 8 - 1),
			];
		} elseif($headers["plength"] > 125) {
			$headers["plength"] = [
				126,
				$headers["plength"] >> 8,
				$headers["plength"] & (2 ** 8 - 1),
			];
		} else {
			$headers["plength"] = [$headers["plength"]];
		}

		$message[1] = $headers["masked"] << 7;
		$message[1] |= array_shift($headers["plength"]);
		array_push($message, ...$headers["plength"]);
		array_push(
			$message,
			...array_map(function($byte) {
				return ord($byte);
			}, str_split($headers["payload"]))
		);

		$message = array_reduce($message, function($lb, $b) {
			return $lb.chr($b);
		});
		return $message;
	}
}