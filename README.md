# PHP WebSocketServer (PHPWS)

PHP itself doesn't support WebSockets but it has the features to enable use of WebSockets.

This project is an inbetween project for a bigger project of mine.

## Getting Started

1. Clone repository or download the latest release<br>
`git clone https://gitlab.com/Playit3110/PHPWS`

2. Implement the class with `onload, onmessage, onerror, onclose`
```php
class WS implements WebSocketServer {
	public function onopen($client) {
		// ...
	}

	public function onmessage($own, $message) {
		// ...
	}

	public function onerror($client, $error) {
		// ...
	}

	public function onclose($client) {
		// ...
	}
}
```

3. Create Instance with
```php
new WS(
	host: "<your-IP>",
	port: <your-port>
);
```

4. Run `php <ws.php>` or open it in the browser
