<?php
require_once(__DIR__."/websocketclient.class.php");
require_once(__DIR__."/frame.class.php");

abstract class WebSocketServer {
	public $host = "0.0.0.0";
	public $port = 8080;
	public $options;
	public $sslOnly;

	protected $server;
	public $max_clients = 20;
	public $sockets = [];
	public $clients = [];
	public $keepOpen = false;

	protected $maxBSize = 2048;

	function __construct(
		$host = "0.0.0.0",
		$port = 8080,
		$options = null,
		$sslOnly = false,
		$maxClients = 20,
		$keepOpen = false,
		$maxBufferSize = 2048
	) {
		$this->host = $host;
		$this->port = $port;
		$this->options = $options;
		$this->sslOnly = $sslOnly;

		$this->max_clients = $maxClients;
		$this->keepOpen = $keepOpen;
		$this->maxBSize = $maxBufferSize;

		try {
			$this->server = stream_socket_server("tcp://".$this->host.":".$this->port, $errno, $errstr, STREAM_SERVER_BIND | STREAM_SERVER_LISTEN);

			if($this->options !== null)
				stream_context_set_option($this->server, $this->options);

			$this->loop();
		} catch(\Throwable $th) {}
	}


	abstract public function onstart();
	abstract public function onopen($client);
	abstract public function tick();
	abstract public function onmessage($client, $message);
	abstract public function onerror($client, $error);
	abstract public function onclose($client);

	protected function loop() {
		$server = new WebSocketClient($this->server, false);
		if($server->socket === false)
			return;

		$this->onstart();

		$hadClients = false;
		while(true) {
			// STOP if process is killed
			if(
				connection_status() !== 0 ||
				(
					!$this->keepOpen &&
					$hadClients &&
					count($this->clients) < 1
				)
			) {
				fclose($this->server);
				break;
			}

			$clients = [
				"server" => $server,
				...$this->clients
			];
			$this->sockets = array_map(function($client) {
				return $client->socket;
			}, $clients);

			$this->tick();

			if(stream_select($this->sockets, $w, $e, 0, 200000) === false) {
				$errno = socket_last_error();
				$errstr = socket_strerror($errno);
			}

			if(in_array($this->server, $this->sockets)) {
				$client = new WebSocketClient($this->server);

				//client couldnt be accepted -> skip
				if(!$client->socket)
					continue;

				$this->clients[] = $client;
				$this->sockets[] = $client->socket;
				$hadClients = true;
			}


			foreach($this->clients as $client) {
				if(!in_array($client->socket, $this->sockets))
					continue;

				stream_set_blocking($client->socket, true);

				$buffer = stream_get_contents($client->socket);
				$nBytes = strlen($buffer);

				if($buffer === false) {
					$error = socket_last_error($client->socket);
					switch($error) {
						case 102: // ENETRESET    -- Network dropped connection because of reset
						case 103: // ECONNABORTED -- Software caused connection abort
						case 104: // ECONNRESET   -- Connection reset by peer
						case 108: // ESHUTDOWN    -- Cannot send after transport endpoint shutdown -- probably more of an error on our part, if we're trying to write after the socket is closed.  Probably not a critical error, though.
						case 110: // ETIMEDOUT    -- Connection timed out
						case 111: // ECONNREFUSED -- Connection refused -- We shouldn't see this one, since we're listening... Still not a critical error.
						case 112: // EHOSTDOWN    -- Host is down -- Again, we shouldn't see this, and again, not critical because it's just one connection and we still want to listen to/for others.
						case 113: // EHOSTUNREACH -- No route to host
						case 121: // EREMOTEIO    -- Rempte I/O error -- Their hard drive just blew up.
						case 125: // ECANCELED    -- Operation canceled
							$client->abort($this, $error);
							break;
						// default:
						// 	display UNCOMMON ERROR
						// 	break;
					}
					continue;
				} elseif($nBytes == 0) {
					$client->disconnect($this);
					continue;
				} else {
					if(!$client->handshake) {
						$client->handshake($buffer, $this);
					} else {
						$client->parsePacket($buffer, $this);
					}
				}
				if(is_resource($client->socket))
					stream_set_blocking($client->socket, false);
			}
		}
	}
}
?>